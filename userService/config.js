exports.config = {
    port: 3000,
    databaseUrl: "mongodb://localhost:27017/",
    database: "morteza",
    TOKEN_SECRET: "bd52f09208dc83c64696bd52f09208dc83c64696",
    services: {
        checkAccess: {
            hostname: "127.0.0.1",
            port: "3001",
            path: "/checkAccess",
            method: "POST",
        },
        checkCaptcha: {
            hostname: "127.0.0.1",
            port: "3002",
            path: "/checkCaptcha",
            method: "POST",
        }
    }
}