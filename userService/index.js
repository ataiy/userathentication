const config = require("./config").config
const http = require('http')
const express = require('express')
const bodyParser = require('body-parser');
const jwt = require('jsonwebtoken');
var crypto = require('crypto');
const app = express()
app.use(bodyParser.json());

var mongo = require('mongodb');
var MongoClient = mongo.MongoClient;
let dbo;
MongoClient.connect(config.databaseUrl, function (err, db) {
    if (err) throw err;
    dbo = db.db(config.database)
    console.log("Database connected.")

    app.listen(config.port, () => {
        console.log(`userService listening at http://localhost:${config.port}`)
    })
});

function requestToService({ serviceName, data }) {
    return new Promise((resolve, reject) => {
        data = JSON.stringify(data)

        let request = http.request({
            ...config.services[serviceName],
            headers: {
                'Content-Type': 'application/json',
                'Content-Length': data.length
            }
        }, result => {
            // console.log(result.statusCode) // in the all time statusCode is 200 
            result.on('data', d => {
                resolve(d)
            })

        })
        request.write(data)
        request.on('error', err => {
            reject(err)
        })
        request.end()
    })
}

function authenticateToken(req, res, next) {
    const authHeader = req.headers['authorization']
    const token = authHeader && authHeader.split(' ')[1]

    if (token == null) return res.sendStatus(401)

    jwt.verify(token, config.TOKEN_SECRET, (err, username) => {
        if (err) {
            console.log(err)
            return res.sendStatus(403)
        }


        // Call check access from adminService
        requestToService({
            serviceName: "checkAccess", data: {
                username,
                path: req.path,
                method: req.method
            }
        }).then(result => {
            if (result == "Ok") {
                // continue if have access
                req.username = username
                next()
            }
            else {
                return res.send(result).status(400)
            }
        }).catch(console.error)
    })
}

app.get('/', authenticateToken, (req, res) => {
    if (req.username) {
        res.send("Athenticated.").status(200)
    }
    else {
        res.redirect('//localhost:3002/captcha').sendStatus(300)
    }
})
app.get('/captcha', (req, res) => {
    res.redirect('//localhost:3002/captcha').sendStatus(300)
})


function checkCaptcha(text) {
    return new Promise((resolve, reject) => {
        // Call check access from adminService
        requestToService({
            serviceName: "checkCaptcha", data: {
                text
            }
        }).then(result => {
            console.log(result.toString())
            if (result == "Ok") {
                // continue if have access
                resolve(result)
            }
            else {
                reject(result)
            }
        }).catch(console.error)

    })
}

app.post('/login', (req, res) => {
    // checkCaptcha(req.body.text).then(() => {
        dbUsers = dbo.collection("users");
        dbUsers.findOne({ username: req.body.username }, (err, user) => {
            if (err) throw err;
            if (!user) {
                res.send("User not found").status(404)
            }
            else {
                if (user.password == crypto.createHash('md5').update(req.body.password).digest('hex')) {
                    res.send({
                        token: jwt.sign(req.body.username, config.TOKEN_SECRET)// , { expiresIn: 1800 } -> have buge: https://github.com/auth0/node-jsonwebtoken/issues/153
                    }).status(200)
                }
                else {
                    res.send("Password is wrong").status(401)
                }
            }
        })
    // }).catch(err => {
    //     res.send(err).status(401)
    // })
})



app.get('/userList', authenticateToken, (req, res) => {
    dbUsers = dbo.collection("users");
    dbUsers.find({}).toArray((err, users) => {
        if (err) throw err;
        if (!users) {
            res.send("Token is not valid").status(401)
        }
        else {
            res.send(users.map(u => ({ ...u, password: null }))).status(200)
        }
    })
})