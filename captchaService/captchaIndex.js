
const express = require('express')
const bodyParser = require('body-parser');
var session = require('express-session');

const app = express()
app.use(bodyParser.json());

app.use(session({
    resave: true,
    saveUninitialized: true,
    secret: 'trolololo'
}));

var svgCaptcha = require('svg-captcha');

app.get('/captcha', function (req, res) {
    var captcha = svgCaptcha.create();
    // console.log(captcha)
    req.session.captcha = captcha.text;

    res.type('svg');
    res.status(200).send(captcha.data);
});

app.post('/checkCaptcha', function (req, res) {
    if ((req.session.captcha ?? "").toUpperCase() == (req.body.text ?? "").toUpperCase()) {
        res.status(200).send("Ok");
    }
    else {
        res.status(400).send("Wrong");
    }
});

app.listen(3002, () => {
    console.log(`captchaService listening at http://localhost:${3002}`)
})