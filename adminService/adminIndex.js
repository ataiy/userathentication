const config = require("./config").config
const express = require('express')
const bodyParser = require('body-parser');
const app = express()
app.use(bodyParser.json());

var mongo = require('mongodb');
var MongoClient = mongo.MongoClient;
let dbo;
MongoClient.connect(config.databaseUrl, function (err, db) {
    if (err) throw err;
    dbo = db.db(config.database)
    console.log("Database connected.")

    app.listen(config.port, () => {
        console.log(`adminService listening at http://localhost:${config.port}`)
    })
});

app.get('/roles', (req, res) => {
    dbRoles = dbo.collection("roles");
    dbRoles.find({}).toArray((err, roles) => {
        if (err) throw err;
        res.send(roles).status(200)
    })
})


app.post('/addOrUpdateRole', (req, res) => {
    dbRoles = dbo.collection("roles");

    dbRoles.update({name: req.body.name}, {$set:{ name: req.body.name, accessList: req.body.accessList }}, { upsert: true }, (err, result) => {
        if (err) throw err;
        res.send("Added successfully.").status(200)
    })
})


app.post('/setUserRole', (req, res) => {
    dbUsers = dbo.collection("users");

    dbUsers.update({ username: req.body.username }, { $set: { roleId: req.body.roleId } }, (err, result) => {
        if (err) throw err;
        res.send("success setted.").status(200)
    })
})

app.post('/checkAccess', (req, res) => {
    dbUsers = dbo.collection("users");
    // dbUsers.aggregate([
    //     {
    //         $lookup:
    //         {
    //             from: 'roles',
    //             localField: 'roleId',
    //             foreignField: '_id',
    //             as: 'role'
    //         }
    //     }
    // ]) // not work
    dbUsers.findOne({ username: req.body.username }, function (err, user) {
        if (err) throw err;
        if (!user) {
            return res.send("User not found").status(404)
        }
        if (!user.roleId) {
            return res.send("Role is not set for this user!").status(400)
        }
        else {
            dbRoles = dbo.collection("roles");
            dbRoles.findOne({ '_id': new mongo.ObjectId(user.roleId) }, (err, role) => {
                if (err) throw err;
                if (!role) { return res.send("Role not found!").status(400) }
                if (role.accessList.find(r => (r.path == req.body.path && r.method == req.body.method))) {
                    req.user = user
                    return res.send("Ok").status(200)
                }
                else {
                    return res.send(`User dont have access to ${req.body.method}: ${req.body.path}`).status(400)
                }
            })
        }
    })
})